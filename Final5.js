    
    function getSplitName (personName) {
    let result = personName.split(" ");
    if(typeof personName !== "string"){
        return "Error: Your input should be string"
    }
    if 
    (result.length == 3) {
        return `firstName = ${result[0]},  middleName = ${result[1]}, lastName = ${result[2]}`  
    }
    if (result.length == 2) {
        return `firstName = ${result[0]},  middleName = null, lastName = ${result[1]}`
    } 
    if (result.length == 1) {
        return `firstName = ${result[0]},  middleName = null, lastName = null`
    }      
        return "This function is only for 3 characters name";
    //early return statement vs if else
}

    console.log (getSplitName("Aldi Danella Pranata"));
    console.log (getSplitName("Dwi Kuncoro"));
    console.log (getSplitName("Aurora"));
    console.log (getSplitName("Aurora Aurellia Sukma Dharma"));
    console.log (getSplitName(0));