//Pembuatan Function
function changeWord ( selectedText, changeText, text){
    let newText = text.replace(selectedText, changeText)
    return newText
}

//Tes data
const kalimat1 = `Adini sangat mencintai kamu selamanya`
const kalimat2 = `Gunung bromo tak akan mampu menggambanrkan besarnya cintaku padamu`

//input data
console.log(changeWord('mencintai', 'membenci', kalimat1))
console.log(changeWord('bromo', 'semeru', kalimat2))

