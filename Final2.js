//Pembuatan Function
function checkTypeNumber(givenNumber){

    //Cek input
    if( givenNumber === undefined){
        return "Bro where is the parameter?"
    }
    else if (typeof givenNumber !== "number" ){
        return "invalid data"
    }
    //proses
    else return givenNumber % 2 == 0 ? "GENAP": "GANJIL"
  }

//input data
console.log(checkTypeNumber(10))
console.log(checkTypeNumber(3))
console.log(checkTypeNumber("3"))
console.log(checkTypeNumber({}))
console.log(checkTypeNumber([]))
console.log(checkTypeNumber())
