function getAngkaTerbesarKedua(dataNumbers) {
    // cek input
    if (!Array.isArray(dataNumbers)) {
      return "Error: Parameter harus berupa array";
    }
    if (dataNumbers.length < 2) {
      return "Error: Array harus memiliki minimal 2 elemen";
    }
    for (let i = 0; i < dataNumbers.length; i++) {
      if (typeof dataNumbers[i] !== 'number') {
        return "Error: Semua elemen array harus bertipe number";
      }
    }
  
    //proses
    const sortedArray = dataNumbers.sort((a, b) => b - a);  
    let secondLargest = sortedArray[1];
    for (let i = 2; i < sortedArray.length; i++) {
      if (sortedArray[i] < secondLargest) {
        break;
      }
      secondLargest = sortedArray[i];
    }
    return secondLargest;
  }
  
    //input
    const dataNumbers = [9,4,7,7,4,3,2,2,8];
    console.log(getAngkaTerbesarKedua(dataNumbers)); 
  
    console.log(getAngkaTerbesarKedua([2, 3, 5, 6, 6, 4])); // Output: 8
  
    console.log(getAngkaTerbesarKedua([0]))
    console.log(getAngkaTerbesarKedua())

  