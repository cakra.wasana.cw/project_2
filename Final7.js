    //test data
    const dataPenjualanPakAldi = [
        {
            namaProduk: 'Sepatu Futsal Nike Vapor Academy 8',
            hargaSatuan: '760000',
            kategori: "Sepatu Sport",
            totalTerjual: '90',
        },
        {
            namaProduk: 'Sepatu Warrior Tristan Black Brown Hight',
            hargaSatuan: '96000',
            kategori: "Sepatu Sneakers" ,
            totalTerjual: '37',
        },
        {
            namaProduk: 'Sepati Warrior Tristan Maroon High',
            hargaSatuan: '96000',
            kategori: "Sepatu Sneakers" ,
            totalTerjual: '90',
        },
        {
            namaProduk: 'Sepatu Warrior Rainbow Tosca Corduroy',
            hargaSatuan: '360000',
            kategori: "Sepatu Sneakers" ,
            totalTerjual: '90',
        }
    ]
    function hitungTotalPenjualan(dataPenjualan) {
        if (!Array.isArray(dataPenjualan)) {
          return "Error: data penjualan harus berupa Array";
        }
      
        let total = 0;
        for (let i = 0; i < dataPenjualan.length; i++) {
          const produk = dataPenjualan[i];
          if (typeof produk.totalTerjual === 'string') {
            const totalTerjual = parseInt(produk.totalTerjual);
            if (!isNaN(totalTerjual)) {
              total += totalTerjual;
            }
          }
        }
      
        return total;
    }
      
      
console.log(hitungTotalPenjualan(dataPenjualanPakAldi))
      