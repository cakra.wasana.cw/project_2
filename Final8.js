const dataPenjualanNovel = [
{
    idProduct: 'BOOK002421',
    namaProduk: 'Pulang - Pergi',
    penulis: 'Tere liye' ,
    hargaBeli: 60000, 
    hargaJual: 86000,
    totalTejual: 150,
    sisaStok:17,
},
{
    idProduct: 'BOOK002351',
    namaProduk: 'Selamat Tinggal',
    penulis: 'Tere liye', 
    hargaBeli: '75000',
    hargaJual: '103000' ,
    totalTejual: 171,
    sisaStok: 20,
},
{
    idProduct: 'BOOK002941',
    namaProduk: 'Garis Waktu',
    penulis:  'Fiersa Besari' , 
    hargaBeli: 67000,
    hargaJual: 99000,
    totalTejual: 213,
    sisaStok: 5,
},
{
    idProduct: 'BOOK002941',
    namaProduk: 'Laskar Pelangi',
    penulis: 'Andrea Hirata' , 
    hargaBeli: 55000,
    hargaJual: 68000,
    totalTejual: 20,
    sisaStok: 56,
},
]

function getInfoPenjualan(dataPenjualan) {
    // Cek Input
    if (!Array.isArray(dataPenjualan)) {
      throw new Error('Parameter harus berupa array');
    }
  
    // Proses
    let totalKeuntungan = 0;
    let totalModal = 0;
    dataPenjualan.forEach((produk) => {
      const modal = produk.hargaBeli * produk.totalTejual;
      const keuntungan = produk.hargaJual * produk.totalTejual - modal;
      totalKeuntungan += keuntungan;
      totalModal += modal;
    });
  
    // Mencari produk buku terlaris dan penulis terlaris
    let produkBukuTerlaris = '';
    let penulisTerlaris = '';
    let maxPenjualan = 0;
    const penulisCount = {};
    dataPenjualan.forEach((produk) => {
      if (produk.totalTejual > maxPenjualan) {
        maxPenjualan = produk.totalTejual;
        produkBukuTerlaris = produk.namaProduk;
      }
      const penulis = produk.penulis;
      penulisCount[penulis] = penulisCount[penulis] ? penulisCount[penulis] + produk.totalTejual : produk.totalTejual;
    });
    penulisTerlaris = Object.keys(penulisCount).reduce((a, b) => penulisCount[a] > penulisCount[b] ? a : b);
  
    // Menghitung persentase keuntungan dari total modal
    const persentaseKeuntungan = `${((totalKeuntungan / totalModal) * 100).toFixed(0)}%`;
  
    // Mengembalikan data dalam bentuk object
    return {
      totalKeuntungan: `Rp. ${totalKeuntungan.toLocaleString('id')}`,
      totalModal: `Rp. ${totalModal.toLocaleString('id')}`,
      persentaseKeuntungan,
      produkBukuTerlaris,
      penulisTerlaris,
    };
  }

  console.log(getInfoPenjualan(dataPenjualanNovel));