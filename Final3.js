function checkEmail(email) {
    // cek input
    if (email === undefined) {
        return"Error: There is no email to check"
    } 
    if(typeof email !== "string"){
        return "Error: The email should be string"
    }
    if(!/[@]/.test(email))
    return "Error: The email should have @ character"

    //Proses
    const emailRegex = new RegExp (/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/)
    const validEmail = emailRegex.test(email)
    let validation
    if (validEmail == true) {
        validation = "VALID"
    } else if (validEmail == false) {
        validation = "INVALID"
    }

    return validation
}

//Input data
console.log(checkEmail('apranata@binar.co.id'))
console.log(checkEmail('apranata@gmail.com'))
console.log(checkEmail('apranata@binar'))
console.log(checkEmail('apranata'))

console.log(checkEmail(3322))

console.log(checkEmail())