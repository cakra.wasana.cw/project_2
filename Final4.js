function isValidPassword (email) {
    //cek input
    if (typeof email == "undefined") {
        return "False Error anda belum memasukkan password" 
    }if (typeof email != "string") {
        return "False Error password tidak berupa string"
    } 
    //proses
    if ( email.length < 8){
        return "False Password kurang dari 8"
    }
    if ( !/[A-Z]/.test(email)){
        return "False Password tidak memiliki huruf besar"
    }
    if ( !/[a-z]/.test(email)){
        return "False Password tidak memiliki huruf kecil"
    }
    if ( !/[0-9]/.test(email)){
        return "False Password tidak memiliki angka"
    }
    return true
    
}
//input
console.log(isValidPassword('Meong2021'))
console.log(isValidPassword('meong2021'))
console.log(isValidPassword('@meong'))
console.log(isValidPassword('@Meong2'))
console.log(isValidPassword('0'))
console.log(isValidPassword())